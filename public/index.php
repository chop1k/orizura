<?php
/**
 * Starting point of the application.
 */

use Orizura\Kernel;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\HttpFoundation\Request;

require dirname(__DIR__).'/vendor/autoload.php';

(new Dotenv())->bootEnv(dirname(__DIR__).'/.env');

/**
 * Starts application.
 *
 * @param Kernel $kernel
 * @param Request $request
 *
 * @throws Exception
 */
function start(Kernel $kernel, Request $request): void
{
    $kernel->terminate($request, $kernel->handle($request)->send());
}

start(new Kernel(), Request::createFromGlobals());
