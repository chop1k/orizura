<?php

use Orizura\Validator\ApplicationValidator;
use Orizura\Validator\BookmarkValidator;
use Orizura\Validator\DirectoryValidator;
use Orizura\Validator\TokenValidator;
use Orizura\Validator\UserValidator;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

/**
 * The function defines routes via {@link RoutingConfigurator}.
 *
 * @param RoutingConfigurator $routes
 */
return static function (RoutingConfigurator $routes): void
{
    $onlyPost = ['POST'];
    $onlyGet = ['GET'];
    $onlyDelete = ['DELETE'];

    $routes
        ->add('token.create', '/api/t/create')
            ->controller(['token.controller', 'create'])
            ->methods($onlyPost)
            ->defaults([
                'validator' => TokenValidator::class,
                'authentication' => 'app'
            ])
    ;

    $routes
        ->add('token.createApp', '/api/t/create/app')
            ->controller(['token.controller', 'createApp'])
            ->methods($onlyPost)
            ->defaults([
                'validator' => TokenValidator::class,
                'authentication' => 'user'
            ])
    ;

    $routes
        ->add('token.get', '/api/t/get')
            ->controller(['token.controller', 'get'])
            ->methods($onlyGet)
    ;

    $routes
        ->add('user.create', '/api/u/create')
            ->controller(['user.controller', 'create'])
            ->methods($onlyPost)
            ->defaults([
                'validator' => UserValidator::class,
            ])
    ;

    $routes
        ->add('application.create', '/api/a/create')
            ->controller(['application.controller', 'create'])
            ->methods($onlyPost)
            ->defaults([
                'validator' => ApplicationValidator::class,
                'authentication' => 'user'
            ])
    ;

    $routes
        ->add('directory.create', '/api/d/create')
            ->controller(['directory.controller', 'create'])
            ->methods($onlyPost)
            ->defaults([
                'validator' => DirectoryValidator::class,
                'authentication' => 'user'
            ])
    ;

    $routes
        ->add('directory.allByOwner', '/api/d/{owner}')
            ->controller(['directory.controller', 'allByOwner'])
            ->methods($onlyGet)
            ->defaults([
                'authentication' => 'user'
            ])
    ;

    $routes
        ->add('directory.deleteById', '/api/d/{id}/delete')
            ->controller(['directory.controller', 'deleteById'])
            ->methods($onlyDelete)
            ->defaults([
                'authentication' => 'user'
            ])
    ;

    $routes
        ->add('directory.edit', '/api/d/{id}/edit')
        ->controller(['directory.controller', 'edit'])
        ->methods($onlyPost)
        ->defaults([
            'validator' => DirectoryValidator::class,
            'authentication' => 'user'
        ])
    ;

    $routes
        ->add('bookmark.create', '/api/b/create')
            ->controller(['bookmark.controller', 'create'])
            ->methods($onlyPost)
            ->defaults([
                'validator' => BookmarkValidator::class,
                'authentication' => 'user'
            ])
    ;

    $routes
        ->add('bookmarks.allByOwner', '/api/b/{owner}')
            ->controller(['bookmark.controller', 'allByOwner'])
            ->methods($onlyGet)
            ->defaults([
                'authentication' => 'user'
            ])
    ;

    $routes
        ->add('bookmarks.deleteById', '/api/b/{id}/delete')
            ->controller(['bookmark.controller', 'deleteById'])
            ->methods($onlyDelete)
            ->defaults([
                'authentication' => 'user'
            ])
    ;

    $routes
        ->add('bookmarks.edit', '/api/b/{id}/edit')
            ->controller(['bookmark.controller', 'edit'])
            ->methods($onlyPost)
            ->defaults([
                'validator' => BookmarkValidator::class,
                'authentication' => 'user'
            ])
    ;
};