<?php

use Orizura\Controller\ApplicationController;
use Orizura\Controller\BookmarkController;
use Orizura\Controller\DirectoryController;
use Orizura\Controller\TokenController;
use Orizura\Controller\UserController;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service;

/**
 * The function defines controllers via {@link ContainerConfigurator}.
 *
 * @param ContainerConfigurator $container
 */
return static function(ContainerConfigurator $container): void
{
    $services = $container->services();

    $authenticationAndEntity = [
        service('db.entity.manager'),
        service('authentication.manager')
    ];

    $services
        ->set('token.controller', TokenController::class)
            ->args($authenticationAndEntity)
    ;

    $services
        ->set('user.controller', UserController::class)
            ->args([
                service('db.entity.manager')
            ])
    ;

    $services
        ->set('application.controller', ApplicationController::class)
            ->args($authenticationAndEntity)
    ;

    $services
        ->set('directory.controller', DirectoryController::class)
            ->args($authenticationAndEntity)
    ;

    $services
        ->set('bookmark.controller', BookmarkController::class)
            ->args($authenticationAndEntity)
    ;
};