<?php

use Orizura\Service\AuthenticationManager;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use function Symfony\Component\DependencyInjection\Loader\Configurator\param;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service;

/**
 * The function defines services via {@link ContainerConfigurator}.
 *
 * @param ContainerConfigurator $configurator
 */
return static function (ContainerConfigurator $configurator): void
{
    $services = $configurator->services();

    $services
        ->set('validation.manager', ValidatorInterface::class)
            ->factory([Validation::class, 'createValidator']);

    $services
        ->set('authentication.manager', AuthenticationManager::class)
            ->args([
                service('db.entity.manager')
            ]);
};