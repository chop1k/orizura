<?php

namespace Orizura\Listener;

use Orizura\Entity\Token;
use Orizura\Exception\AuthenticationException;
use Orizura\Exception\LogicException;
use Orizura\Exception\ValidationException;
use Orizura\Service\AuthenticationManager;
use Orizura\Validator\ControllerValidator;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\Exception\JsonException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Loader\PhpFileLoader as RouterPhpLoader;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class RequestListener listens the {@link KernelEvents::REQUEST} event.
 *
 * @package Orizura\Listener
 */
class RequestListener
{
    /**
     * FileLocator locates the config directory.
     *
     * @var FileLocator $locator
     */
    protected FileLocator $locator;

    /**
     * Validation service needed for validation.
     *
     * @var ValidatorInterface $validator
     */
    protected ValidatorInterface $validator;

    /**
     * Authentication manager for authentication.
     *
     * @var AuthenticationManager $authentication
     */
    protected AuthenticationManager $authentication;

    /**
     * RequestListener constructor.
     *
     * @param FileLocator $locator
     *
     * @param ValidatorInterface $validator
     *
     * @param AuthenticationManager $authentication
     */
    public function __construct(FileLocator $locator, ValidatorInterface $validator, AuthenticationManager $authentication)
    {
        $this->locator = $locator;
        $this->validator = $validator;
        $this->authentication = $authentication;
    }

    /**
     * Executes when {@link KernelEvents::REQUEST} event dispatched.
     *
     * @param RequestEvent $event
     *
     * @throws ValidationException
     *
     * @throws AuthenticationException
     *
     * @throws LogicException
     */
    public function onRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();

        if ($request->getMethod() === 'OPTIONS')
        {
            $event->setResponse(new Response('OK', 200));

            return;
        }

        $this->handleRoutes($request);
        $this->handleJson($request);
        $this->handleValidation($request);
        $this->handleAuthentication($request);
    }

    /**
     * Gets routes from the configuration file and matches it with the request url.
     *
     * @param Request $request
     */
    protected function handleRoutes(Request $request): void
    {
        $context = (new RequestContext())->fromRequest($request);

        $routes = (new RouterPhpLoader($this->locator))->load('routes.php');

        $request->attributes->replace(
            (new UrlMatcher($routes, $context))->matchRequest($request)
        );
    }

    /**
     * If the request is a json request, it parses the request body and passes the result to the request bag.
     *
     * @param Request $request
     */
    protected function handleJson(Request $request): void
    {
        if ($request->headers->get('content-type', false) === 'application/json' && $request->getMethod() === 'POST')
        {
            $json = json_decode($request->getContent(), true);

            if (is_null($json))
            {
                throw new JsonException('Cannot decode given data.');
            }

            $request->request->replace($json);
        }
    }

    /**
     * If the controller has a validator, it processes the request data through the validator.
     *
     * @param Request $request
     *
     * @throws ValidationException
     */
    protected function handleValidation(Request $request): void
    {
        /**
         * @var ControllerValidator $validator
         */
        $validator = $request->attributes->get('validator', false);

        if ($validator === false || !class_exists($validator))
        {
            return;
        }

        if (!in_array(ControllerValidator::class, class_implements($validator)))
        {
            return;
        }

        $methods = $validator::getAliases();
        $method = $request->attributes->get('_controller')[1];

        if (!isset($methods[$method]))
        {
            return;
        }

        $method = $methods[$method];

        /**
         * @var ConstraintViolationList $violations
         */
        $violations = $this->validator->validate($request->request->all(), $validator::$method());

        if ($violations->count() > 0)
        {
            throw new ValidationException($violations);
        }
    }

    /**
     * Authenticates user if 'authenticated' attribute specified.
     *
     * @param Request $request
     *
     * @throws AuthenticationException
     *
     * @throws LogicException
     */
    protected function handleAuthentication(Request $request): void
    {
        if (!$request->attributes->has('authentication'))
        {
            return;
        }

        if ($request->cookies->has('token'))
        {
            $rawToken = $request->cookies->get('token', false);
        }
        elseif ($request->headers->has('token'))
        {
            $rawToken = $request->headers->get('token', false);
        }
        else
        {
            throw AuthenticationException::authenticationFailed();
        }

        $token = $this->authentication->authenticateByToken($rawToken);

        if (is_null($token))
        {
            throw AuthenticationException::authenticationFailed();
        }

        $type = $request->attributes->get('authentication');

        if ($type === 'user')
        {
            $this->authenticateUser($token);
        }

        $request->attributes->set('token', $token);
    }

    /**
     * Handles user token.
     *
     * @param Token $token
     *
     * @throws AuthenticationException
     */
    private function authenticateUser(Token $token): void
    {
        if (is_null($token->user))
        {
            throw AuthenticationException::authenticationTypeNotAllowed('user');
        }
    }
}