<?php

namespace Orizura\Listener;

use Orizura\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class ExceptionListener listens {@link KernelEvents::EXCEPTION} event.
 *
 * @package Orizura\Listener
 */
class ExceptionListener
{
    /**
     * Executes when {@link KernelEvents::EXCEPTION} event dispatched.
     * Converts the exception to the {@link JsonResponse}.
     *
     * @param ExceptionEvent $event
     */
    public function onException(ExceptionEvent $event): void
    {
        $throwable = $event->getThrowable();

        if (!($throwable instanceof Exception))
        {
            $throwable = Exception::fromThrowable($throwable);
        }

        $event->setResponse(new JsonResponse($throwable, $throwable->getStatus()));
    }
}