<?php

namespace Orizura\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Orizura\Entity\Application;
use Orizura\Entity\Directory;
use Orizura\Entity\Token;
use Orizura\Entity\User;
use Orizura\Exception\LogicException;
use Orizura\Exception\NotFoundException;

/**
 * Class AuthenticationManager manages authentication.
 *
 * @package Orizura\Service
 */
class AuthenticationManager
{
    /**
     * Entity manager for interactions with database.
     *
     * @var EntityManager $entity
     */
    protected EntityManager $entity;

    /**
     * AuthenticationManager constructor.
     *
     * @param EntityManager $entity
     */
    public function __construct(EntityManager $entity)
    {
        $this->entity = $entity;
    }

    /**
     * Authenticates by given username and password.
     *
     * @param string $name
     *
     * @param string $password
     *
     * @return int
     * Returns user id if found.
     *
     * @throws LogicException
     *
     * @throws NotFoundException
     */
    public function authenticateByPassword(string $name, string $password): int
    {
        $query = $this->entity->getRepository(User::class)
            ->createNativeNamedQuery('authenticate');

        $query->setParameters([
            'name' => $name,
            'password' => $password
        ]);

        try {
            return $query->getSingleScalarResult();
        } catch (NoResultException $exception) {
            throw NotFoundException::userNotFound($name);
        } catch (NonUniqueResultException $exception) {
            throw LogicException::userNotUnique($name);
        }
    }

    /**
     * Authenticates by given token identifier.
     *
     * @param string $token
     *
     * @return object|null
     * Returns token object or null if not found.
     *
     * @throws LogicException
     */
    public function authenticateByToken(string $token): ?Token
    {
        $query = $this->entity->getRepository(Token::class)
            ->createNativeNamedQuery('authenticate');

        $query->setParameters([
            'id' => $token
        ]);

        try {
            return $query->getOneOrNullResult();
        } catch (NonUniqueResultException $exception) {
            throw LogicException::queryReturnedSeveralIds('token');
        }
    }

    /**
     * Checks if application belongs to the owner.
     *
     * @param int $owner
     *
     * @param int $app
     *
     * @return bool
     *
     * @throws LogicException
     *
     * @throws NotFoundException
     */
    public function authenticateAppByOwner(int $owner, int $app): bool
    {
        $query = $this->entity->getRepository(Application::class)
            ->createNativeNamedQuery('authenticate');

        $query->setParameters([
            'id' => $app
        ]);

        try {
            return $query->getSingleScalarResult() === $owner;
        } catch (NoResultException $e) {
            throw NotFoundException::applicationNotFound($app);
        } catch (NonUniqueResultException $e) {
            throw LogicException::foundSeveralEntities($app, 'applications');
        }
    }

    /**
     * Checks if directory belongs to the to whoever requests.
     *
     * @param int $user
     *
     * @param int $directory
     *
     * @return bool
     *
     * @throws LogicException
     *
     * @throws NotFoundException
     */
    public function authenticateUserOwnsDirectory(int $user, int $directory): bool
    {
        $query = $this->entity->getRepository(Directory::class)
            ->createNativeNamedQuery('authenticate');

        $query->setParameters([
            'id' => $directory
        ]);

        try {
            return $query->getSingleScalarResult() === $user;
        } catch (NoResultException $e) {
            throw NotFoundException::directoryNotFound($directory);
        } catch (NonUniqueResultException $e) {
            throw LogicException::foundSeveralEntities($directory, 'directories');
        }
    }
}