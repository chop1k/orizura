<?php

namespace Orizura\Controller;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Orizura\Entity\Directory;
use Orizura\Entity\Token;
use Orizura\Exception\AuthenticationException;
use Orizura\Exception\LogicException;
use Orizura\Exception\NotFoundException;
use Orizura\Service\AuthenticationManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DirectoryController handles directory-related requests.
 *
 * @package Orizura\Controller
 */
class DirectoryController
{
    /**
     * Entity manager for interactions with database.
     *
     * @var EntityManager $entity
     */
    protected EntityManager $entity;

    /**
     * Authentication manager for authentication.
     *
     * @var AuthenticationManager $authentication
     */
    protected AuthenticationManager $authentication;

    /**
     * DirectoryController constructor.
     *
     * @param EntityManager $entity
     *
     * @param AuthenticationManager $authentication
     */
    public function __construct(EntityManager $entity, AuthenticationManager $authentication)
    {
        $this->entity = $entity;
        $this->authentication = $authentication;
    }

    /**
     * Creates new directory.
     *
     * @param Request $request
     *
     * @param Token $token
     *
     * @return Response
     * Returns json response with id.
     *
     * @throws AuthenticationException
     *
     * @throws LogicException
     *
     * @throws NotFoundException
     */
    public function create(Request $request, Token $token): Response
    {
        $owner = $token->user;

        $parent = $request->request->get('parent');

        if (!is_null($parent))
        {
            if (!$this->authentication->authenticateUserOwnsDirectory($owner, $parent))
            {
                throw AuthenticationException::authenticationDirByOwnerFailed($owner, $parent);
            }
        }

        $query = $this->entity->getRepository(Directory::class)
            ->createNativeNamedQuery('create');

        $query->setParameters([
            'name' => $request->request->get('name'),
            'parent' => $parent,
            'owner' => $owner
        ]);

        try {
            return new JsonResponse([
                'id' => $query->getSingleScalarResult()
            ]);
        } catch (NoResultException $e) {
            throw LogicException::queryNotReturnedEntityId('directory');
        } catch (NonUniqueResultException $e) {
            throw LogicException::queryReturnedSeveralIds('directories');
        }
    }

    /**
     * Searches all bookmarks owns by given owner identifier.
     *
     * @param Token $token
     *
     * @param int $owner
     *
     * @return Response
     * Returns json response with array of bookmarks.
     *
     * @throws AuthenticationException
     */
    public function allByOwner(Token $token, int $owner): Response
    {
        $user = $token->user;

        if ($user !== $owner)
        {
            throw AuthenticationException::notEnoughRights();
        }

        $query = $this->entity->getRepository(Directory::class)
            ->createNativeNamedQuery('allByOwner');

        $query->setParameters([
            'owner' => $owner
        ]);

        return new JsonResponse($query->getArrayResult());
    }

    /**
     * Deletes directory with given id and owner.
     *
     * @param Token $token
     *
     * @param int $id
     *
     * @return Response
     */
    public function deleteById(Token $token, int $id): Response
    {
        $query = $this->entity->getRepository(Directory::class)
            ->createNativeNamedQuery('deleteById');

        $query->setParameters([
            'id' => $id,
            'owner' => $token->user
        ]);

        $query->execute();

        return new Response('OK', 200);
    }

    /**
     * Edits directory.
     *
     * @param Request $request
     *
     * @param Token $token
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit(Request $request, Token $token, int $id): Response
    {
        $query = $this->entity->getRepository(Directory::class)
            ->createNativeNamedQuery('edit');

        $query->setParameters([
            'id' => $id,
            'name' => $request->request->get('name'),
            'parent' => $request->request->get('parent'),
            'owner' => $token->user
        ]);

        $query->execute();

        return new Response('OK', 200);
    }
}