<?php

namespace Orizura\Controller;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Orizura\Entity\User;
use Orizura\Exception\LogicException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UserController handles user-related requests.
 *
 * @package Orizura\Controller
 */
class UserController
{
    /**
     * Entity manager for interactions with database.
     *
     * @var EntityManager $entity
     */
    protected EntityManager $entity;

    /**
     * UserController constructor.
     *
     * @param EntityManager $entity
     */
    public function __construct(EntityManager $entity)
    {
        $this->entity = $entity;
    }

    /**
     * Creates user by given data and returns json response with id.
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws LogicException
     */
    public function create(Request $request): Response
    {
        $query = $this->entity->getRepository(User::class)
            ->createNativeNamedQuery('create');

        $query->setParameters([
            'name' => $request->request->get('name'),
            'email' => $request->request->get('email'),
            'password' => $request->request->get('password')
        ]);

        try {
            return new JsonResponse([
                'id' => $query->getSingleScalarResult()
            ]);
        } catch (NoResultException $e) {
            throw LogicException::queryNotReturnedEntityId('user');
        } catch (NonUniqueResultException $e) {
            throw LogicException::queryReturnedSeveralIds('users');
        }
    }
}