<?php

namespace Orizura\Controller;

use Doctrine\ORM\EntityManager;
use Orizura\Entity\Token;
use Orizura\Exception\AuthenticationException;
use Orizura\Exception\LogicException;
use Orizura\Exception\NotFoundException;
use Orizura\Service\AuthenticationManager;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class TokenController handles token-related requests.
 *
 * @package Orizura\Controller
 */
class TokenController
{
    /**
     * Entity manager for interactions with database.
     *
     * @var EntityManager $entity
     */
    protected EntityManager $entity;

    /**
     * Authentication manager for authentication.
     *
     * @var AuthenticationManager $authentication
     */
    protected AuthenticationManager $authentication;

    /**
     * TokenController constructor.
     *
     * @param EntityManager $entity
     *
     * @param AuthenticationManager $authentication
     */
    public function __construct(EntityManager $entity, AuthenticationManager $authentication)
    {
        $this->entity = $entity;
        $this->authentication = $authentication;
    }

    /**
     * Authenticates user, generates id and creates a new token.
     *
     * @param Request $request
     *
     * @param Token $token
     *
     * @return Response
     * Returns json response with the token.
     *
     * @throws LogicException
     *
     * @throws NotFoundException
     */
    public function create(Request $request, Token $token): Response
    {
        $user = $this->authentication->authenticateByPassword(
            $request->request->get('name'),
            $request->request->get('password')
        );

        return $this->createToken([
            'user' => $user,
            'app' => $token->app
        ]);
    }

    /**
     * Authenticates application and creates application token.
     *
     * @param Request $request
     *
     * @param Token $token
     *
     * @return Response
     * Returns json response with the token.
     *
     * @throws AuthenticationException
     *
     * @throws LogicException
     *
     * @throws NotFoundException
     */
    public function createApp(Request $request, Token $token): Response
    {
        $owner = $token->user;

        $app = $request->request->get('app');

        if ($this->authentication->authenticateAppByOwner($owner, $app))
        {
            throw AuthenticationException::authenticateAppByOwnerFailed($owner, $app);
        }

        return $this->createToken([
            'user' => null,
            'app' => $request->request->get('app')
        ]);
    }

    /**
     * Creates token and returns json response.
     *
     * @param array $params
     *
     * @return JsonResponse
     */
    private function createToken(array $params): JsonResponse
    {
        $query = $this->entity->getRepository(Token::class)
            ->createNativeNamedQuery('create');

        $tokenId = Uuid::uuid4()->toString();

        $params['id'] = $tokenId;

        $query->setParameters($params);

        $query->execute();

        return new JsonResponse([
            'token' => $tokenId
        ]);
    }

    /**
     * Returns info about token.
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws AuthenticationException
     *
     * @throws LogicException
     */
    public function get(Request $request): Response
    {
        if ($request->cookies->has('token'))
        {
            $rawToken = $request->cookies->get('token', false);
        }
        elseif ($request->headers->has('token'))
        {
            $rawToken = $request->headers->get('token', false);
        }
        else
        {
            return new JsonResponse([
                'id' => null
            ]);
        }

        $token = $this->authentication->authenticateByToken($rawToken);

        if (is_null($token))
        {
            throw AuthenticationException::authenticationFailed();
        }

        $response = [
            'user' => $token->user,
            'app' => $token->app
        ];

        if ($request->query->has('returnId'))
        {
            $response['id'] = $token->id;
        }

        return new JsonResponse($response);
    }
}
