<?php

namespace Orizura\Controller;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Orizura\Entity\Application;
use Orizura\Entity\Token;
use Orizura\Exception\LogicException;
use Orizura\Service\AuthenticationManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ApplicationController handles applications-related requests.
 *
 * @package Orizura\Controller
 */
class ApplicationController
{
    /**
     * Entity manager for interactions with database.
     *
     * @var EntityManager $entity
     */
    protected EntityManager $entity;

    /**
     * Authentication manager for authentication.
     *
     * @var AuthenticationManager $authentication
     */
    protected AuthenticationManager $authentication;

    /**
     * ApplicationController constructor.
     *
     * @param EntityManager $entity
     *
     * @param AuthenticationManager $authentication
     */
    public function __construct(EntityManager $entity, AuthenticationManager $authentication)
    {
        $this->entity = $entity;
        $this->authentication = $authentication;
    }

    /**
     * Creates a new application.
     *
     * @param Request $request
     *
     * @param Token $token
     *
     * @return Response
     * Returns id of the application.
     *
     * @throws LogicException
     */
    public function create(Request $request, Token $token): Response
    {
        $query = $this->entity->getRepository(Application::class)
            ->createNativeNamedQuery('create');

        $query->setParameters([
            'name' => $request->request->get('name'),
            'owner' => $token->user
        ]);

        try {
            return new JsonResponse([
                'id' => $query->getSingleScalarResult()
            ]);
        } catch (NoResultException $e) {
            throw LogicException::queryNotReturnedEntityId('application');
        } catch (NonUniqueResultException $e) {
            throw LogicException::queryReturnedSeveralIds('applications');
        }
    }
}