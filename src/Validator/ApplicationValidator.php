<?php

namespace Orizura\Validator;

use Orizura\Controller\ApplicationController;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

/**
 * Class ApplicationValidator defines the validation rules for {@link ApplicationController}.
 *
 * @package Orizura\Validator
 */
class ApplicationValidator implements ControllerValidator
{
    /**
     * Returns collection of validation rules for the create action of the {@link ApplicationController}.
     *
     * @return Collection
     */
    public static function create(): Collection
    {
        return new Collection([
            'name' => [
                new Type('string'),
                new Length(['max' => 32]),
                new NotBlank()
            ]
        ]);
    }

    /**
     * @inheritDoc
     */
    public static function getAliases(): array
    {
        return [
            'create' => 'create'
        ];
    }
}