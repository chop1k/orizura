<?php

namespace Orizura\Validator;

use Orizura\Controller\DirectoryController;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

/**
 * Class DirectoryValidator defines the validation rules for {@link DirectoryController}.
 *
 * @package Orizura\Validator
 */
class DirectoryValidator implements ControllerValidator
{
    /**
     * Returns collection of validation rules for the create action of the {@link DirectoryController}.
     *
     * @return Collection
     */
    public static function create(): Collection
    {
        return new Collection([
            'name' => [
                new Type('string'),
                new Length(['max' => 36]),
                new NotBlank()
            ],
            'parent' => [
                new Type('integer'),
                new GreaterThan(0),
            ]
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function getAliases(): array
    {
        return [
            'create' => 'create',
            'edit' => 'create'
        ];
    }
}