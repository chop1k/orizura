<?php

namespace Orizura\Validator;

use Orizura\Controller\UserController;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

/**
 * Class UserValidator defines the validation rules for {@link UserController}.
 *
 * @package Orizura\Validator
 */
class UserValidator implements ControllerValidator
{
    /**
     * Returns collection of validation rules for the create action of the {@link UserController}.
     *
     * @return Collection
     */
    public static function create(): Collection
    {
        return new Collection([
            'name' => [
                new Type('string'),
                new Length(['max' => 32]),
                new NotBlank()
            ],
            'email' => [
                new Type('string'),
                new Length(['max' => 64]),
                new Email(),
                new NotBlank()
            ],
            'password' => [
                new Type('string'),
                new Length(['max' => 64]),
                new NotBlank()
            ]
        ]);
    }

    /**
     * @inheritDoc
     */
    public static function getAliases(): array
    {
        return [
            'create' => 'create'
        ];
    }
}