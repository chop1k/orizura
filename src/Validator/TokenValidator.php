<?php

namespace Orizura\Validator;

use Orizura\Controller\TokenController;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

/**
 * Class TokenValidator defines the validation rules for {@link TokenController}.
 *
 * @package Orizura\Validator
 */
class TokenValidator implements ControllerValidator
{
    /**
     * Returns collection of validation rules for the create action of the {@link TokenController}.
     *
     * @return Collection
     */
    public static function create(): Collection
    {
        return new Collection([
            'name' => [
                new Type('string'),
                new Length(['max' => 32]),
                new NotBlank()
            ],
            'password' => [
                new Type('string'),
                new Length(['max' => 64]),
                new NotBlank()
            ]
        ]);
    }

    /**
     * Returns collection of validation rules for the createApp action of the {@link TokenController}.
     *
     * @return Collection
     */
    public static function createApp(): Collection
    {
        return new Collection([
            'app' => [
                new Type('integer'),
                new GreaterThan(0),
                new NotBlank()
            ]
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function getAliases(): array
    {
        return [
            'create' => 'create',
            'createApp' => 'createApp',
        ];
    }
}