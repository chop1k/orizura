<?php

namespace Orizura\Validator;

use Orizura\Controller\BookmarkController;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Constraints\Url;

/**
 * Class BookmarkValidator defines the validation rules for {@link BookmarkController}.
 *
 * @package Orizura\Validator
 */
class BookmarkValidator implements ControllerValidator
{
    /**
     * Returns collection of validation rules for the create action of the {@link BookmarkController}.
     *
     * @return Collection
     */
    public static function create(): Collection
    {
        return new Collection([
            'name' => [
                new Type('string'),
                new Length(['max' => 128]),
                new NotBlank()
            ],
            'url' => [
                new Type('string'),
                new Length(['max' => 512]),
                new Url(),
                new NotBlank()
            ],
            'directory' => [
                new Type('integer'),
                new GreaterThan(0),
                new NotBlank()
            ],
            'description' => [
                new Type('string'),
                new Length(['max' => 512])
            ]
        ]);
    }

    /**
     * @inheritDoc
     */
    public static function getAliases(): array
    {
        return [
            'create' => 'create',
            'edit' => 'create'
        ];
    }
}