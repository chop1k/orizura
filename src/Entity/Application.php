<?php

namespace Orizura\Entity;

use DateTime;
use Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\MappingException;

/**
 * Class Application represents an entity in the database.
 *
 * @package Orizura\Entity
 */
class Application
{
    /**
     * Application unique identifier.
     *
     * @var int $id
     */
    public int $id;

    /**
     * Application name.
     *
     * @var string $name
     */
    public string $name;

    /**
     * Application owner id.
     *
     * @var int $owner
     */
    public int $owner;

    /**
     * Application creation timestamp.
     *
     * @var DateTime $creationTimestamp
     */
    public DateTime $creationTimestamp;

    /**
     * Loads entity metadata.
     *
     * @param ClassMetadata $metadata
     *
     * @throws MappingException
     */
    public static function loadMetadata(ClassMetadata $metadata): void
    {
        $metadata->setPrimaryTable([
            'name' => '"Application"'
        ]);

        self::loadBuilder(new ClassMetadataBuilder($metadata));
        self::loadQueries($metadata);
    }

    /**
     * Builds entity structure via {@link ClassMetadataBuilder}.
     *
     * @param ClassMetadataBuilder $builder
     */
    protected static function loadBuilder(ClassMetadataBuilder $builder)
    {
        $builder
            ->createField('id', 'integer')
                ->columnName('id')
                ->makePrimaryKey()
                ->unique(true)
                ->nullable(false)
            ->build()
        ;

        $builder
            ->createField('name', 'string')
                ->columnName('name')
                ->length(32)
                ->unique(false)
                ->nullable(false)
            ->build()
        ;

        $builder
            ->createField('owner', 'integer')
                ->columnName('owner')
                ->unique(false)
                ->nullable(false)
            ->build()
        ;

        $builder
            ->createField('creation_timestamp', 'datetime')
                ->columnName('creation_timestamp')
                ->unique(false)
                ->nullable(false)
            ->build()
        ;

    }

    /**
     * Loads a named queries and resultSetMappings.
     *
     * @param ClassMetadata $metadata
     *
     * @throws MappingException
     */
    protected static function loadQueries(ClassMetadata $metadata)
    {
        $metadata->addNamedNativeQuery([
            'name' => 'create',
            'query' => 'insert into "API"."Application" (name, owner) values (:name, :owner) returning id',
            'resultSetMapping' => 'createResult'
        ]);

        $metadata->addSqlResultSetMapping([
            'name' => 'createResult',
            'entities' => [
                'application' => [
                    'entityClass' => self::class
                ]
            ]
        ]);

        $metadata->addNamedNativeQuery([
            'name' => 'authenticate',
            'query' => 'select owner from "API"."Application" where id = :id',
            'resultSetMapping' => 'authenticateResult'
        ]);

        $metadata->addSqlResultSetMapping([
            'name' => 'authenticateResult',
            'entities' => [
                'application' => [
                    'entityClass' => self::class
                ]
            ]
        ]);
    }
}