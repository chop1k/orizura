<?php

namespace Orizura\Entity;

use DateTime;
use Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\MappingException;

/**
 * Class Directory represents a directory entity in the database.
 *
 * @package Orizura\Entity
 */
class Directory
{
    /**
     * Directory unique identifier.
     *
     * @var int $id
     */
    public int $id;

    /**
     * Name of a directory.
     *
     * @var string $name
     */
    public string $name;

    /**
     * Parent of a directory.
     *
     * @var int|null $parent
     */
    public ?int $parent;

    /**
     * Owner of a directory.
     *
     * @var int $owner
     */
    public int $owner;

    /**
     * Creation timestamp of a directory.
     *
     * @var DateTime $creationTimestamp
     */
    public DateTime $creationTimestamp;

    /**
     * Loads entity metadata.
     *
     * @param ClassMetadata $metadata
     *
     * @throws MappingException
     */
    public static function loadMetadata(ClassMetadata $metadata)
    {
        $metadata->setPrimaryTable([
            'name' => '"Directory"'
        ]);

        self::loadBuilder(new ClassMetadataBuilder($metadata));
        self::loadQueries($metadata);
    }

    /**
     * Builds entity structure via {@link ClassMetadataBuilder}.
     *
     * @param ClassMetadataBuilder $builder
     */
    protected static function loadBuilder(ClassMetadataBuilder $builder)
    {
        $builder
            ->createField('id', 'integer')
                ->makePrimaryKey()
                ->columnName('id')
                ->unique(true)
                ->nullable(false)
            ->build()
        ;

        $builder
            ->createField('name', 'string')
                ->columnName('name')
                ->unique(false)
                ->nullable(false)
                ->length(36)
            ->build()
        ;

        $builder
            ->createField('parent', 'integer')
                ->columnName('parent')
                ->unique(false)
                ->nullable(true)
            ->build()
        ;

        $builder
            ->createField('owner', 'integer')
                ->columnName('owner')
                ->unique(false)
                ->nullable(false)
            ->build()
        ;

        $builder
            ->createField('creation_timestamp', 'datetime')
                ->columnName('creation_timestamp')
                ->unique(false)
                ->nullable(false)
            ->build()
        ;
    }

    /**
     * Loads a named queries and resultSetMappings.
     *
     * @param ClassMetadata $metadata
     *
     * @throws MappingException
     */
    protected static function loadQueries(ClassMetadata $metadata)
    {
        $metadata->addNamedNativeQuery([
            'name' => 'create',
            'query' => 'insert into "API"."Directory" (name, parent, owner) values (:name, :parent, :owner) returning id',
            'resultSetMapping' => 'createResult'
        ]);

        $metadata->addSqlResultSetMapping([
            'name' => 'createResult',
            'entities' => [
                'directory' => [
                    'entityClass' => self::class
                ]
            ]
        ]);

        $metadata->addNamedNativeQuery([
            'name' => 'authenticate',
            'query' => 'select owner from "API"."Directory" where id = :id',
            'resultSetMapping' => 'authenticateResult'
        ]);

        $metadata->addSqlResultSetMapping([
            'name' => 'authenticateResult',
            'entities' => [
                'directory' => [
                    'entityClass' => self::class
                ]
            ]
        ]);

        $metadata->addNamedNativeQuery([
            'name' => 'allByOwner',
            'query' => 'select * from "API"."Directory" where owner = :owner',
            'resultSetMapping' => 'allByOwnerResult'
        ]);

        $metadata->addSqlResultSetMapping([
            'name' => 'allByOwnerResult',
            'entities' => [
                'directories' => [
                    'entityClass' => self::class
                ]
            ]
        ]);

        $metadata->addNamedNativeQuery([
            'name' => 'deleteById',
            'query' => 'delete from "API"."Directory" where id = :id and owner = :owner',
            'resultSetMapping' => 'deleteByIdResult'
        ]);

        $metadata->addSqlResultSetMapping([
            'name' => 'deleteByIdResult',
        ]);

        $metadata->addNamedNativeQuery([
            'name' => 'edit',
            'query' => 'update "API"."Directory" set name = :name, parent = :parent, owner = :owner where id = :id and owner = :owner',
            'resultSetMapping' => 'editResult'
        ]);

        $metadata->addSqlResultSetMapping([
            'name' => 'editResult',
        ]);
    }
}