<?php

namespace Orizura\Entity;

use DateTime;
use Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\MappingException;

/**
 * Class Bookmark represents a directory entity in the database.
 *
 * @package Orizura\Entity
 */
class Bookmark
{
    /**
     * Unique identifier of the bookmark.
     *
     * @var int $id
     */
    public int $id;

    /**
     * Name of the bookmark.
     *
     * @var string $name
     */
    public string $name;

    /**
     * Url of the bookmark.
     *
     * @var string $url
     */
    public string $url;

    /**
     * Directory of the bookmark.
     *
     * @var int $directory
     */
    public int $directory;

    /**
     * Owner of the bookmark.
     *
     * @var int $owner
     */
    public int $owner;

    /**
     * Creation timestamp of the bookmark.
     *
     * @var DateTime $creationTimestamp
     */
    public DateTime $creationTimestamp;

    /**
     * Description of the bookmark.
     *
     * @var string $description
     */
    public string $description;

    /**
     * Load entity metadata.
     *
     * @param ClassMetadata $metadata
     *
     * @throws MappingException
     */
    public static function loadMetadata(ClassMetadata $metadata)
    {
        $metadata->setPrimaryTable([
            'name' => '"Bookmark"'
        ]);

        self::loadBuilder(new ClassMetadataBuilder($metadata));
        self::loadQueries($metadata);
    }

    /**
     * Builds entity structure via {@link ClassMetadataBuilder}.
     *
     * @param ClassMetadataBuilder $builder
     */
    protected static function loadBuilder(ClassMetadataBuilder $builder)
    {
        $builder
            ->createField('id', 'integer')
                ->makePrimaryKey()
                ->columnName('id')
                ->nullable(false)
                ->unique(true)
            ->build()
        ;

        $builder
            ->createField('name', 'string')
                ->columnName('name')
                ->length(128)
                ->nullable(false)
                ->unique(false)
            ->build()
        ;

        $builder
            ->createField('url', 'string')
                ->columnName('url')
                ->length(512)
                ->nullable(false)
                ->unique(false)
            ->build()
        ;

        $builder
            ->createField('directory', 'integer')
                ->columnName('directory')
                ->nullable(false)
                ->unique(false)
            ->build()
        ;

        $builder
            ->createField('owner', 'integer')
                ->columnName('owner')
                ->nullable(false)
                ->unique(false)
            ->build()
        ;

        $builder
            ->createField('creationTimestamp', 'datetime')
                ->columnName('creation_date')
                ->nullable(false)
                ->unique(false)
            ->build()
        ;

        $builder
            ->createField('description', 'string')
                ->columnName('description')
                ->length(512)
                ->nullable(true)
                ->unique(false)
            ->build()
        ;
    }

    /**
     * Loads a named queries and resultSetMappings.
     *
     * @param ClassMetadata $metadata
     *
     * @throws MappingException
     */
    protected static function loadQueries(ClassMetadata $metadata)
    {
        $metadata->addNamedNativeQuery([
            'name' => 'create',
            'query' => 'insert into "API"."Bookmark" (name, url, description, directory, owner) values (:name, :url, :description, :directory, :owner) returning id',
            'resultSetMapping' => 'createResult'
        ]);

        $metadata->addSqlResultSetMapping([
            'name' => 'createResult',
            'entities' => [
                'bookmarks' => [
                    'entityClass' => self::class
                ]
            ]
        ]);

        $metadata->addNamedNativeQuery([
            'name' => 'allByOwner',
            'query' => 'select * from "API"."Bookmark" where owner = :owner',
            'resultSetMapping' => 'allByOwnerResult'
        ]);

        $metadata->addSqlResultSetMapping([
            'name' => 'allByOwnerResult',
            'entities' => [
                'bookmarks' => [
                    'entityClass' => self::class
                ]
            ]
        ]);

        $metadata->addNamedNativeQuery([
            'name' => 'deleteById',
            'query' => 'delete from "API"."Bookmark" where id = :id and owner = :owner',
            'resultSetMapping' => 'deleteByIdResult'
        ]);

        $metadata->addSqlResultSetMapping([
            'name' => 'deleteByIdResult'
        ]);

        $metadata->addNamedNativeQuery([
            'name' => 'edit',
            'query' => 'update "API"."Bookmark" set name = :name, url = :url, directory = :directory, description = :description where id = :id and owner = :owner',
            'resultSetMapping' => 'editResult'
        ]);

        $metadata->addSqlResultSetMapping([
            'name' => 'editResult',
        ]);

    }
}