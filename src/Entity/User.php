<?php

namespace Orizura\Entity;

use DateTime;
use Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\MappingException;

/**
 * Class User represents an entity in the database.
 *
 * @package Orizura\Entity
 */
class User
{
    /**
     * User identifier.
     *
     * @var int $id
     */
    public int $id;

    /**
     * User name.
     *
     * @var string $name
     */
    public string $name;

    /**
     * User email.
     *
     * @var string $email
     */
    public string $email;

    /**
     * User password.
     *
     * @var string $password
     */
    public string $password;

    /**
     * User creation timestamp;
     *
     * @var DateTime $creationTimestamp
     */
    public DateTime $creationTimestamp;

    /**
     * Loads entity metadata.
     *
     * @param ClassMetadata $metadata
     *
     * @throws MappingException
     */
    public static function loadMetadata(ClassMetadata $metadata)
    {
        $metadata->setPrimaryTable([
            'name' => '"User"'
        ]);

        self::loadBuilder(new ClassMetadataBuilder($metadata));
        self::loadQueries($metadata);
    }

    /**
     * Builds entity structure via {@link ClassMetadataBuilder}.
     *
     * @param ClassMetadataBuilder $builder
     */
    protected static function loadBuilder(ClassMetadataBuilder $builder)
    {
        $builder
            ->createField('id', 'integer')
                ->columnName('id')
                ->makePrimaryKey()
                ->unique(true)
                ->nullable(false)
            ->build()
        ;

        $builder
            ->createField('name', 'string')
                ->columnName('name')
                ->nullable(false)
                ->unique(true)
                ->length(32)
            ->build()
        ;

        $builder
            ->createField('email', 'string')
                ->columnName('email')
                ->unique(true)
                ->nullable(false)
                ->length(64)
            ->build()
        ;

        $builder
            ->createField('password', 'string')
                ->columnName('password')
                ->nullable(false)
                ->unique(false)
                ->length(64)
            ->build()
        ;

        $builder
            ->createField('creation_timestamp', 'timestamp')
                ->columnName('creation_timestamp')
                ->nullable(false)
                ->unique(false)
            ->build()
        ;
    }

    /**
     * Loads a named queries and resultSetMappings.
     *
     * @param ClassMetadata $metadata
     *
     * @throws MappingException
     */
    protected static function loadQueries(ClassMetadata $metadata)
    {
        $metadata->addNamedNativeQuery([
            'name' => 'create',
            'query' => 'insert into "API"."User" (name, email, password) values (:name, :email, :password) returning id',
            'resultSetMapping' => 'createResult'
        ]);

        $metadata->addSqlResultSetMapping([
            'name' => 'createResult',
            'entities' => [
                'user' => [
                    'entityClass' => self::class
                ]
            ]
        ]);

        $metadata->addNamedNativeQuery([
            'name' => 'authenticate',
            'query' => 'select id from "API"."User" where name = :name and password = :password',
            'resultSetMapping' => 'authenticateResult'
        ]);

        $metadata->addSqlResultSetMapping([
            'name' => 'authenticateResult',
            'entities' => [
                'user' => [
                    'entityClass' => self::class
                ]
            ]
        ]);
    }
}