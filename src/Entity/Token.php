<?php

namespace Orizura\Entity;

use DateTime;
use Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\MappingException;

/**
 * Class Token represents a token entity in the database.
 *
 * @package Orizura\Entity
 */
class Token
{
    /**
     * Token unique identifier.
     *
     * @var string $id
     */
    public string $id;

    /**
     * User which owns this token.
     *
     * @var int|null $user
     */
    public ?int $user;

    public int $app;

    /**
     * Timestamp of creation.
     *
     * @var DateTime $creationTimestamp
     */
    public DateTime $creationTimestamp;

    /**
     * Loads entity metadata.
     *
     * @param ClassMetadata $metadata
     *
     * @throws MappingException
     */
    public static function loadMetadata(ClassMetadata $metadata): void
    {
        $metadata->setPrimaryTable([
            'name' => '"Token"'
        ]);

        self::loadBuilder(new ClassMetadataBuilder($metadata));
        self::loadQueries($metadata);
    }

    /**
     * Builds entity structure via {@link ClassMetadataBuilder}.
     *
     * @param ClassMetadataBuilder $builder
     */
    protected static function loadBuilder(ClassMetadataBuilder $builder): void
    {
        $builder
            ->createField('id', 'string')
                ->makePrimaryKey()
                ->unique(true)
                ->nullable(false)
                ->length(36)
            ->build()
        ;

        $builder
            ->createField('user', 'integer')
                ->columnName('user')
                ->unique(false)
                ->nullable(true)
            ->build()
        ;

        $builder
            ->createField('app', 'integer')
                ->columnName('app')
                ->unique(false)
                ->nullable(false)
            ->build()
        ;

        $builder
            ->createField('creationTimestamp', 'datetime')
                ->columnName('creation_timestamp')
                ->unique(false)
                ->nullable(false)
            ->build()
        ;
    }

    /**
     * Loads a named queries and resultSetMappings.
     *
     * @param ClassMetadata $metadata
     *
     * @throws MappingException
     */
    protected static function loadQueries(ClassMetadata $metadata)
    {
        $metadata->addNamedNativeQuery([
            'name' => 'create',
            'query' => 'insert into "API"."Token" (id, "user", app) values (:id, :user, :app)',
            'resultSetMapping' => 'createResult'
        ]);

        $metadata->addSqlResultSetMapping([
            'name' => 'createResult',
            'entities' => [
                'token' => [
                    'entityClass' => self::class
                ]
            ]
        ]);

        $metadata->addNamedNativeQuery([
            'name' => 'authenticate',
            'query' => 'select * from "API"."Token" where id = :id',
            'resultSetMapping' => 'authenticateResult'
        ]);

        $metadata->addSqlResultSetMapping([
            'name' => 'authenticateResult',
            'entities' => [
                'token' => [
                    'entityClass' => self::class
                ]
            ]
        ]);
    }
}