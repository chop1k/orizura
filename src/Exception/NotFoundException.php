<?php

namespace Orizura\Exception;

class NotFoundException extends Exception
{
    /**
     * Shortcut for creating the exception with certain message, code and status.
     *
     * @param string $name
     *
     * @return NotFoundException
     */
    public static function userNotFound(string $name): NotFoundException
    {
        return new NotFoundException(
            sprintf('User with name %s not found. ', $name), 404
        );
    }

    /**
     * Shortcut for creating the exception with certain message, code and status.
     *
     * @param int $id
     *
     * @return NotFoundException
     */
    public static function applicationNotFound(int $id): NotFoundException
    {
        return new NotFoundException(
            sprintf('Application with id %s not found. ', $id), 404
        );
    }

    /**
     * Shortcut for creating the exception with certain message, code and status.
     *
     * @param int $id
     *
     * @return NotFoundException
     */
    public static function directoryNotFound(int $id): NotFoundException
    {
        return new NotFoundException(
            sprintf('Directory with id %s not found. ', $id), 404
        );
    }
}