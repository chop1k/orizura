<?php

namespace Orizura\Exception;

use Exception as BaseException;
use JsonSerializable;
use ReflectionClass;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

/**
 * Class Exception represents exception which will be converted to the {@link JsonResponse}.
 *
 * @package Orizura\Exception
 */
class Exception extends BaseException implements JsonSerializable
{
    /**
     * Contains {@link Response} status.
     *
     * @var int $status
     */
    protected int $status;

    /**
     * Returns {@link Response} status.
     *
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * Sets {@link Response} status.
     *
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @inheritdoc
     */
    public function __construct($message, int $status = 500, $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->status = $status;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize(): array
    {
        $json = [
            'message' => $this->getMessage(),
            'code' => $this->getCode(),
            'type' => (new ReflectionClass($this))->getShortName(),
            'data' => []
        ];

        if ($_ENV['APP_DEBUG'])
        {
            $json['trace'] = $this->getTrace();
        }

        return $json;
    }

    /**
     * Shortcut for creating exception from another exception or {@link Throwable}.
     *
     * @param Throwable $throwable
     *
     * @param int $status
     *
     * @return static
     */
    public static function fromThrowable(Throwable $throwable, int $status = 500): self
    {
        $exception = new Exception(
            $throwable->getMessage(),
            $throwable->getCode()
        );

        $exception->setStatus($status);

        return $exception;
    }
}