<?php

namespace Orizura\Exception;

class LogicException extends Exception
{
    /**
     * Shortcut for creating the exception with certain message, code and status.
     *
     * @param string $name
     *
     * @return LogicException
     */
    public static function userNotUnique(string $name): LogicException
    {
        return new LogicException(
            sprintf('Found several users with name %s. ', $name), 500
        );
    }

    /**
     * Shortcut for creating the exception with certain message, code and status.
     *
     * @param int $id
     *
     * @param string $entity
     *
     * @return LogicException
     */
    public static function foundSeveralEntities(int $id, string $entity): LogicException
    {
        return new LogicException(
            sprintf('Found several %s with id %s. ', $entity, $id), 500
        );
    }

    /**
     * Shortcut for creating the exception with certain message, code and status.
     *
     * @param string $name
     *
     * @return LogicException
     */
    public static function queryNotReturnedEntityId(string $name): LogicException
    {
        return new LogicException(
            sprintf('Query not returned %s id', $name), 500
        );
    }

    /**
     * Shortcut for creating the exception with certain message, code and status.
     *
     * @param string $name
     *
     * @return LogicException
     */
    public static function queryReturnedSeveralIds(string $name): LogicException
    {
        return new LogicException(
            sprintf('Query returned several %s id. ', $name), 500
        );
    }
}