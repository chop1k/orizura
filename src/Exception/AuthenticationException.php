<?php

namespace Orizura\Exception;

/**
 * Class AuthenticationException represents authentication exception.
 *
 * @package Orizura\Exception
 */
class AuthenticationException extends Exception
{
    /**
     * Shortcut for creating the exception with certain message, code and status.
     *
     * @param int $owner
     *
     * @param int $app
     *
     * @return AuthenticationException
     */
    public static function authenticateAppByOwnerFailed(int $owner, int $app): AuthenticationException
    {
        return new AuthenticationException(
            sprintf('Authentication for application %s failed. User %s does not own this application. ', $app, $owner), 403
        );
    }

    /**
     * Shortcut for creating the exception with certain message, code and status.
     *
     * @return AuthenticationException
     */
    public static function authenticationFailed(): AuthenticationException
    {
        return new AuthenticationException('This action needs authentication.', 403);
    }

    /**
     * Shortcut for creating the exception with certain message, code and status.
     *
     * @param string $type
     *
     * @return AuthenticationException
     */
    public static function authenticationTypeNotAllowed(string $type): AuthenticationException
    {
        return new AuthenticationException(
            sprintf('This action needs token of %s type.', $type), 403
        );
    }

    /**
     * Shortcut for creating the exception with certain message, code and status.
     *
     * @param int $owner
     *
     * @param int $directory
     *
     * @return AuthenticationException
     */
    public static function authenticationDirByOwnerFailed(int $owner, int $directory): AuthenticationException
    {
        return new AuthenticationException(
            sprintf('Authentication for directory %s failed. User %s does not own this directory. ', $directory, $owner), 403
        );
    }

    /**
     * Shortcut for creating the exception with certain message, code and status.
     *
     * @return AuthenticationException
     */
    public static function notEnoughRights(): AuthenticationException
    {
        return new AuthenticationException('Not enough rights for this action. ', 403);
    }
}